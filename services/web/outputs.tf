output "mod_clb_dns_name" {
  value       = aws_elb.mod_elb.dns_name
  description = "The domain name of the load balancer"
}

output "mod_asg_name" {
  value       = aws_autoscaling_group.mod_auto_scaling_group.name
  description = "The name of the Auto Scaling Group in the module"
}