# *********************************************************************************************************************
# Owner         :   Indra G. Harijono
# Created       :   Fri, 10/18/2019
# 
# Require       :   Terraform 0.12 or higher
#                   AWS Account: IAM User with access key Id and Key
#                                use:
#                                   AWS_ACCESS_KEY_ID=AKIA3HDCOW2D3HFGSLPG
#                                   AWS_SECRET_ACCESS_KEY=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
# 
# Description   :   variable (generic and input) for module file
# Creates variable to be used as input parameter
# of the module and other variables for general use in 
# the module itself.
# *********************************************************************************************************************



# ---------------------------------------------------------------------------------------------------------------------
# ENVIRONMENT VARIABLES
# Define these secrets as environment variables
# ---------------------------------------------------------------------------------------------------------------------

# AWS_ACCESS_KEY_ID
# AWS_SECRET_ACCESS_KEY

# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# ---------------------------------------------------------------------------------------------------------------------

variable "mod_cluster_name" {
  description = "The name to use for all the cluster resources"
  type        = string
}

variable "mod_instance_type" {
  description = "The type of EC2 Instances to run (e.g. t2.micro)"
  type        = string
}

variable "mod_min_size" {
  description = "The minimum number of EC2 Instances in the ASG"
  type        = number
}

variable "mod_max_size" {
  description = "The maximum number of EC2 Instances in the ASG"
  type        = number
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# ---------------------------------------------------------------------------------------------------------------------

variable "mod_server_port" {
  description = "The port the server will use for HTTP requests"
  type        = number
  default     = 8080
}

variable "mod_elb_port" {
  description = "The port the ELB will use for HTTP requests"
  type        = number
  default     = 80
}